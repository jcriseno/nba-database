var mysql   = require('mysql');
var db  = require('./db_connection.js');

/* DATABASE CONFIGURATION */
var connection = mysql.createConnection(db.config);

/*
 create or replace view skill_view as
 select s.*, a.street, a.zip_code from skill s
 join skill a on a.skill_id = s.skill_id;

 */

exports.getAll = function(callback) {
    var query = 'SELECT * FROM player_stats';

    connection.query(query, function(err, result) {
        callback(err, result);
    });
};