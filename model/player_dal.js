var mysql   = require('mysql');
var db  = require('./db_connection.js');

/* DATABASE CONFIGURATION */
var connection = mysql.createConnection(db.config);

/*
 create or replace view skill_view as
 select s.*, a.street, a.zip_code from skill s
 join skill a on a.skill_id = s.skill_id;

 */

exports.getPass = function(callback) {
    var query = 'SELECT * FROM player_assists';

    connection.query(query, function(err, result){
        callback(err, result);
    });
};

exports.getAll = function(callback) {
    var query = 'SELECT * FROM player';

    connection.query(query, function(err, result) {
        callback(err, result);
    });
};

exports.getById = function(player_id, callback) {
    var query = 'SELECT * FROM player WHERE player_id = ?';
    var queryData = [player_id];
    console.log(query);

    connection.query(query, queryData, function(err, result) {

        callback(err, result);
    });
};

exports.insert = function(params, callback) {

    // FIRST INSERT THE skill
    var query = 'INSERT INTO player (name, number, position, college, age) VALUES (?,?,?,?,?)';

    var queryData = ([params.name, params.number, params.position, params.college, params.age]);


    // NOTE THE EXTRA [] AROUND skillskillData
    connection.query(query, queryData, function(err, result){
        callback(err, result);
    });
//    });

};