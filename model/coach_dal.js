var mysql   = require('mysql');
var db  = require('./db_connection.js');

/* DATABASE CONFIGURATION */
var connection = mysql.createConnection(db.config);

/*
 create or replace view skill_view as
 select s.*, a.street, a.zip_code from skill s
 join skill a on a.skill_id = s.skill_id;

 */

exports.getAll = function(callback) {
    var query = 'SELECT * FROM coach';

    connection.query(query, function(err, result) {
        callback(err, result);
    });
};

exports.getById = function(coach_id, callback) {
    var query = 'SELECT * FROM coach WHERE coach_id = ?';
    var queryData = [coach_id];
    console.log(query);

    connection.query(query, queryData, function(err, result) {

        callback(err, result);
    });
};