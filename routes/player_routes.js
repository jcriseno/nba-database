var express = require('express');
var router = express.Router();
var player_dal = require('../model/player_dal');

router.get('/playerassists', function(req, res) {
    player_dal.getPass(function(err, result){
        console.log(result);
        if(err) {
            res.send(err);
        }
        else {
            res.render('player/playerAssists', { 'result':result });
        }
    });

});


router.get('/all', function(req, res) {
    player_dal.getAll(function(err, result){
        console.log(result);
        if(err) {
            res.send(err);
        }
        else {
            res.render('player/playerViewAll', { 'result':result });
        }
    });

});

router.get('/', function(req, res){
    if(req.query.player_id == null) {
        res.send('player_id is null');
    }
    else {
        player_dal.getById(req.query.player_id, function(err,result) {
            if (err) {
                res.send(err);
            }
            else {
                res.render('player/playerViewById', {'result': result});
            }
        });
    }
});

router.get('/add', function(req, res){
    // passing all the query parameters (req.query) to the insert function instead of each individually
    player_dal.getAll(function(err,result) {
        if (err) {
            res.send(err);
        }
        else {
            res.render('player/playerAdd', {'player': result});
        }
    });
});

router.get('/insert', function(req, res){
    // simple validation
    if(req.query.name == null) {
        res.send('Name must be provided.');
    }
    else if(req.query.number == null) {
        res.send('At least one number must be selected');
    }
    else if (req.query.position == null) {
        res.send('At least one position must be selected');
    }
    else if (req.query.college == null) {
        res.send('At least one college must be selected');
    }
    else if (req.query.age == null) {
        res.send('At least one age must be selected');
    }
    else {
        // passing all the query parameters (req.query) to the insert function instead of each individually
        player_dal.insert(req.query, function(err,result) {
            if (err) {
                console.log(err)
                res.send(err);
            }
            else {
                //poor practice for redirecting the user to a different page, but we will handle it differently once we start using Ajax
                res.redirect(302, '/player/all');
            }
        });
    }
});

module.exports = router;