var express = require('express');
var router = express.Router();
var playerstats_dal = require('../model/playerstats_dal');

// View All skills
router.get('/all', function(req, res) {
    playerstats_dal.getAll(function(err, result){
        console.log(result);
        if(err) {
            res.send(err);
        }
        else {
            res.render('playerstats/playerstatsViewAll', { 'result':result });
        }
    });

});

module.exports = router;