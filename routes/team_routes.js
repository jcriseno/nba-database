var express = require('express');
var router = express.Router();
var team_dal = require('../model/team_dal');

// View All skills
router.get('/all', function(req, res) {
    team_dal.getAll(function(err, result){
        console.log(result);
        if(err) {
            res.send(err);
        }
        else {
            res.render('team/teamViewAll', { 'result':result });
        }
    });

});


router.get('/', function(req, res){
    if(req.query.team_id == null) {
        res.send('team_id is null');
    }
    else {
        team_dal.getById(req.query.team_id, function(err,result) {
            if (err) {
                res.send(err);
            }
            else {
                res.render('team/teamViewById', {'result': result});
            }
        });
    }
});

router.get('/edit', function(req, res){
    if(req.query.team_id == null) {
        res.send('A team id is required');
    }
    else {
        team_dal.edit(req.query.team_id, function(err, result){
            res.render('team/teamUpdate', {team: result[0]});
        });
    }

});

router.get('/update', function(req, res) {
    team_dal.update(req.query, function(err, result){
        res.redirect(302, '/team/all');
    });
});

module.exports = router;
