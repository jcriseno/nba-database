var express = require('express');
var router = express.Router();
var coach_dal = require('../model/coach_dal');

// View All skills
router.get('/all', function(req, res) {
    coach_dal.getAll(function(err, result){
        console.log(result);
        if(err) {
            res.send(err);
        }
        else {
            res.render('coach/coachViewAll', { 'result':result });
        }
    });

});

router.get('/', function(req, res){
    if(req.query.coach_id == null) {
        res.send('coach_id is null');
    }
    else {
        coach_dal.getById(req.query.coach_id, function(err,result) {
            if (err) {
                res.send(err);
            }
            else {
                res.render('coach/coachViewById', {'result': result});
            }
        });
    }
});

module.exports = router;